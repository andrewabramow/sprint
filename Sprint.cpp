/*������� 1. ������ �� 100 ������

��� ����� �������

���������� ��������� ���������� �� ������� �� 100 ������.
����� ����� ������� � ����� �������. ����� ������� 
����������� ������� �� ������������ �����. ������ �� ���
����� � ������ ���������, ������� ����� �������
������������� ��� ������, ����� ����������� ���� �
������ � �������.

������ ������� � ����������� ����� ������� ���������� �
���, ������� ������� ��� ��� ���� ������.

��� ������ ��� ������ ��������� 100-�������� �������,
������ ������������� � ��������� ������� � ���������
������������, ��������������� �� ����������� ���������
������� �������.

������ � ������������

����������� ���� ��� ���������� �������� ������� ������ � �����������.
*/

#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <string>

// access 
std::mutex finish_access;

// main object
struct Athlete {
    std::string name = "unknown";
    double speed = 0;
    double distance = 0;
};

void every_sec_check(Athlete& man, std::vector<Athlete>&results) {

    int int_sec = 100 / (int)man.speed;
    double remaind_sec = (100 / man.speed) - (double)int_sec;

    auto as_duration = std::chrono::duration_cast<std::chrono::steady_clock::duration>
        (std::chrono::duration<double>(remaind_sec));

    for (int i = 0; i < int_sec; ++i) {  //report every second
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout << std::endl << "Name: " << man.name << std::endl <<
            "Distance: " << man.speed * (i+1) << std::endl;
    }
    std::this_thread::sleep_for(as_duration);  //last sector without report
    std::cout << std::endl << man.name << " FINISHED" << std::endl;

    finish_access.lock();
    results.push_back(man);
    finish_access.unlock();
}

int main()
{
    Athlete* swimmers = new Athlete[6];
    std::vector <Athlete> results;

    // initialize swimmers
    for (int i = 0; i < 6; ++i) {
        std::cout << "Please input name & speed of the "<<i+1<<" athlete\n";
        std::cin >> swimmers[i].name;
        std::cin >> swimmers[i].speed;
    }

    std::cout << "Competition started\n";

    std::thread swimmer1(every_sec_check, std::ref(swimmers[0]), ref(results));
    std::thread swimmer2(every_sec_check, std::ref(swimmers[1]), ref(results));
    std::thread swimmer3(every_sec_check, std::ref(swimmers[2]), ref(results));
    std::thread swimmer4(every_sec_check, std::ref(swimmers[3]), ref(results));
    std::thread swimmer5(every_sec_check, std::ref(swimmers[4]), ref(results));
    std::thread swimmer6(every_sec_check, std::ref(swimmers[5]), ref(results));
    swimmer1.join();
    swimmer2.join();
    swimmer3.join();
    swimmer4.join();
    swimmer5.join();
    swimmer6.join();

    //output result
    std::cout << "Name / Time\n";
    for (int i = 0; i < results.size(); ++i) {
        std::cout << results[i].name << " : " <<
            std::setprecision(2)<<100 / results[i].speed << std::endl;
    }
}

